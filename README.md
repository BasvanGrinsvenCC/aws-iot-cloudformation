# AWS IoT CloudFormation

This project is meant for supporting our blog about how we connect an ESP32 to AWS IoT.
In this project are 2 YAML files which represent CloudFormation stacks.

First you should spin up the 'roles-policies' stack and then the 'middleware' stack.

Good luck and happy coding!